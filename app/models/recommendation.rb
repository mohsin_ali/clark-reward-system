class Recommendation < ApplicationRecord

  validates :recommendee, :recommender, presence: true, allow_blank: false

  def self.get_recommender_of(recommendee)
    where(recommendee: recommendee).order("created_at asc").first.try(:recommender)
  end

end
