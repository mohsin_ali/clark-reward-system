module RewardsHelper  
  ######################################
  ## Function:  parse_recommends_string
  ## @param:    input_string
  ##            It accepts a string with keyword 'recommends'.
  ##            Parses it and extracts out the recommender and recommendee
  def parse_recommends_string input_string
    output = []

    index = input_string.index(Settings.recommends)
    output.push(input_string[index - 2].strip)                              # recommender
    output.push(input_string[index + Settings.recommends.size + 1].strip) # recommendee

    return output
  end

  ######################################
  ## Function:  parse_accepts_string
  ## @param:    input_string
  ##            It accepts a string with keyword 'accepts'.
  ##            Parses it and extracts out the recommendee
  def parse_accepts_string input_string    
    index = input_string.index(Settings.accepts)

    recommendee = input_string[index - 2].strip # recommendee
  end
end