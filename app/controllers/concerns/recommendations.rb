module Recommendations
  extend ActiveSupport::Concern

  ## On Accepting a recommendation, the recommender should be awarded.
  def accept_recommendation recommendee
    level = 0
    x = recommendee
    while(recommender = Recommendation.get_recommender_of(x))
      set_score(recommender, level)
      x = recommender
      level += 1
    end
  end

  ## Update recommender's score
  def set_score recommender, level
    @scores[recommender] = @scores[recommender] + calculate_score_by_level(level)
  end

  ## Calculate score 
  def calculate_score_by_level(level)
    (1.0/2.0)**level
  end
end