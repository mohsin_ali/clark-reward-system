class Api::V1::RewardsController < ApplicationController
  include Recommendations
  include RewardsHelper
  
  ## Clean all old input data
  before_action :clean_input_data

  ## ##########################################
  ## GET/rewards
  ## @params
  ##      @data:  contains text file with input data
  def index    
    @scores = {}

    ## Return if input data file is not provided.
    return render json: { error: true, msg: 'Input data file is required.' }, status: 422 if params[:data].blank?
    
    ## Open file and read contents line by line
    File.open(params[:data].path, "r").each_line do |line|      
      
      ## #########################################
      ## CASE: Input is a recommendation
      if line.include?(Settings.recommends)
        recommender, recommendee = parse_recommends_string(line)

        Recommendation.create(recommender: recommender, recommendee: recommendee)                        
        
        ## Initialize the recommender's score with 0
        @scores[recommender] = 0 unless @scores.has_key?(recommender)

      ## #########################################
      ## CASE: Input is an acceptance
      elsif line.include?(Settings.accepts)
        recommendee = parse_accepts_string(line)
        
        accept_recommendation(recommendee)
      end
    end

    return render json: @scores
  end

  private
    def clean_input_data
      Recommendation.destroy_all
    end

    
end
