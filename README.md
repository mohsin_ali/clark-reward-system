# Reward System - CLARK

This is an assignment task for calculating rewards based on recommendations. 

## Technical Specs  
* Ruby 2.4.1
* Rails 5.2.3
* Postgresql

## Setup  
* Clone repository: `git clone git@bitbucket.org:mohsin_ali/clark-reward-system.git`
* Inside project folder run: `bundle install`
* Run `rails db:migrate`
* Run `rails s`

## Test Suite
Run `rails test`

In order to test the API, Postman can be used. API doc can be found at `https://documenter.getpostman.com/view/262458/SVSKKTsm?version=latest`  
