class CreateRecommendations < ActiveRecord::Migration[5.2]
  def change
    create_table :recommendations do |t|
      t.string :recommender
      t.string :recommendee

      t.timestamps
    end
  end
end
