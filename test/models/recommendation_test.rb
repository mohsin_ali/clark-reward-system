require 'test_helper'

class RecommendationTest < ActiveSupport::TestCase
  test "return the right recommender" do
    recommender_A = Recommendation.get_recommender_of("B")
    recommender_B = Recommendation.get_recommender_of("C")
    recommender_C = Recommendation.get_recommender_of("D")
    
    assert_equal "A", recommender_A
    assert_equal "B", recommender_B
    assert_equal "C", recommender_C
  end
end
